const mongoose = require('mongoose');
 
//Attributes of the Course object
var productSchema = new mongoose.Schema({
    productName: {
        type: String,
        required:'this field is required'
    },
    productPrice: {
        type: Number,
        required:'this field is required'
    },
    categoryId: {
        type: String,
        required: 'this field is required'
    }
});
 
mongoose.model('Product', productSchema);