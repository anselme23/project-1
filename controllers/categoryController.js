//Import the dependencies
const adminMiddleware = require("../middlewares/admin")
const express = require('express');
const mongoose = require('mongoose');
const _=require("lodash")

//Creating a Router
var router = express.Router();
//Create Course model CLASS
const Category = mongoose.model('Category');
 


//Router Controller for CREATE request
router.post('/',adminMiddleware,(req,res) => {
    /////const token = req.header('x-auth-token')
    //if(!token) return res.send('not authenticated..').status(401)
    insertIntoMongoDB(req, res);
});

 //Router Controller for UPDATE request

router.put('/',adminMiddleware,(req,res) => {
    updateIntoMongoDB(req, res);
});
     
//Creating function to insert data into MongoDB
function insertIntoMongoDB(req,res) {
    let category = new Category(_.pick(req.body,['categoryName','categoryDescription']))
    category.save()
        .then(categorySaved => res.send(categorySaved).status(201))
        .catch(err => res.send(err).status(400));
}
 
//Creating a function to update data in MongoDB
function updateIntoMongoDB(req, res) {
    Category.findOneAndUpdate({ _id: req.body._id },req.body, { new: true })
        .then(category => res.send(category))
        .catch(err => res.send(err).status(400));
} 


//Router to retrieve the complete list of available courses
router.get('/',(req,res) => {
    Category.find()
        .then(categories => res.send(categories))
        .catch(err => res.send(err).status(404));
});


//Router to get a course using it's ID
router.get('/:id', (req, res) => {
    Category.findById(req.params.id)
        .then(course => res.send(course))
        .catch(err => res.send(err).status(404));
});
 

//Router Controller for DELETE request
router.delete('/:id',adminMiddleware, (req, res) => {
    Category.findByIdAndRemove(req.params.id)
        .then(category => res.send(category))
        .catch(err => res.send(err).status(404));
});
module.exports = router;