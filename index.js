require('./model/mongodb')
const productController = require('./controllers/productController');
const categoryController = require('./controllers/categoryController');
const userController = require("./controllers/UserController")
const authMiddleware = require('./middlewares/auth')
const accountController = require("./controllers/accountController")
const config = require('config')
//Import the necessary packages
const express = require('express');

var app = express();
const bodyparser = require('body-parser');
 
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
 
if(!config.get("jwtPrivateKey")){
    console.log('JWT PRIVATE KEY IS NOT DEFINED')
    process.exit(1)
} 
//Create a welcome message and direct them to the main page
app.get('/', (req, res) => {
    res.send('Welcome to our app');
});

//Set the Controller path which will be responding the user actions

app.use('/api/products',authMiddleware,productController)
app.use('/api/categories',authMiddleware,categoryController)
app.use('/api/users',userController)
app.use('/api/accounts',authMiddleware,accountController)



const port = process.env.PORT || 2000;
app.listen(port, () => console.log(`Listening on port ${port}..`));
 
